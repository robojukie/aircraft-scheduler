import React from "react";

function Flight({ flight, toggleRotation }) {
  return (
    <div className="flight-card" onClick={() => toggleRotation(flight)}>
      <div className="flight-header">{flight.id}</div>
      <div className="flight-details-container">
        <div className="flight-details">
          <div>{flight.origin}</div>
          <div>{flight.readable_departure}</div>
        </div>
        <div className="flight-details">
          <div>{flight.destination}</div>
          <div>{flight.readable_arrival}</div>
        </div>
      </div>
    </div>
  );
}

export default Flight;
