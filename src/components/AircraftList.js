import React from "react";
import { useEffect, useState } from "react";

function AircraftList({ aircrafts, flights }) {
  const [utilizationPercentage, setUtilizationPercentage] = useState(0);

  function calculateUtilization() {
    const TOTAL_SECONDS_IN_DAY = 86400;
    let totalFlightDuration = 0;
    flights.forEach((flight) => {
      let flightDuration = flight.arrivaltime - flight.departuretime;
      totalFlightDuration += flightDuration;
    });

    const utilizationPercentage =
      (totalFlightDuration / TOTAL_SECONDS_IN_DAY) * 100;
    setUtilizationPercentage(utilizationPercentage);
  }

  useEffect(() => calculateUtilization());

  return (
    <div className="side-panel">
      <header className="panel-header">
        <p>Aircrafts</p>
      </header>
      <div className="side-panel-list">
        {aircrafts?.map((aircraft) => {
          return (
            <div key={aircraft.ident} className="aircraft-card">
              <div>{aircraft.ident}</div>
              <div>{Math.round(utilizationPercentage)} %</div>
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default AircraftList;
