import React from "react";
import Flight from "./Flight";

function ScheduledFlightList({ flights, aircraft, toggleRotation }) {
  return (
    <div className="center-panel">
      <header className="panel-header">
        <p>Rotation {aircraft}</p>
      </header>
      <div className="center-panel-list">
        {flights?.map((flight) => {
          return (
            <Flight
              key={flight.id}
              flight={flight}
              toggleRotation={toggleRotation}
            />
          );
        })}
      </div>
    </div>
  );
}

export default ScheduledFlightList;
