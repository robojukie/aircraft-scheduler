import React from "react";

function AircraftTimeline({ inFlightTimes }) {
  /*
  ex: 
  [[21600, 26100], [35100, 40500]] by s
  [[15, 18.125], [20.833333333333332, 24.375], [34.583333333333336, 38.75]] by m

  flight times will already be sorted by departure time

  render green div for numbers between a pair
  render gray div from 0 to lowest 0 index
  render gray div from largest 1 index to 86400
  render purple div for numbers not in pair ranges
  */

  /** todo: account for flights just past midnight - round those to 1 instead of 0 */
  const firstDepartureTime = inFlightTimes[0]?.[0];
  const endOfFirstIdleSegment = Math.round(firstDepartureTime || 0); // first dep time of sorted flightTimes
  const lastArrivalTime =
    inFlightTimes.length > 0 ? inFlightTimes[inFlightTimes.length - 1][1] : 0; // last arrival time of sorted flights
  const startOfLastIdleSegment = Math.round(1440 - lastArrivalTime);

  // todo: move styling out
  return (
    <div
      className="bar-container"
      style={{
        height: "30px",
        width: "360px", // todo: width = total min, divided by 4 just to shrink:  1440...fix this later to something better
        background: "white",
        display: "flex",
        flexDirection: "row",
        // justifyContent: "space-evenly"
      }}
    >
      {/* ideally would just render color conditionally depending i think */}
      {/* render first idle segment, 1%= 1 min */}
      {[...Array(endOfFirstIdleSegment)]?.map((e, i) => (
        <div
          key={i}
          className="idleTime"
          style={{
            height: "100%",
            width: "1%",
            background: "gray",
          }}
        ></div>
      ))}
      {/* todo figure out how to calculate and render middle section */}
      {/* {[...Array()]?.map((e, i) => (
        <div
          key={i}
          className="idleTime"
          style={{
            height: "100px",
            width: "1%",
            background: "green",
          }}
        ></div>
      ))} */}

      {/* render back idle segment */}
      {[...Array(startOfLastIdleSegment)]?.map((e, i) => (
        <div
          key={i}
          className="idleTime"
          style={{
            height: "100%",
            width: "1%",
            background: "dark gray",
          }}
        ></div>
      ))}
    </div>
  );
}

export default AircraftTimeline;
