import React from "react";

import Flight from "./Flight";

function FlightList({ flights, toggleRotation }) {
  // let flightSet = new Set();
  // flights.map((flight) => {
  //   if (flightSet.has(flight.id)) {
  //     console.log(flight.id);
  //   } else {
  //     flightSet.add(flight.id);
  //   }
  //   console.log("done");
  //   return flightSet;
  // });

  flights.sort((a, b) => (a.id > b.id ? 1 : -1));

  return (
    <div className="side-panel">
      <header className="panel-header">
        <p>Flights</p>
      </header>
      <div className="side-panel-list">
        {flights?.map((flight) => {
          return (
            <Flight
              key={flight.id}
              flight={flight}
              toggleRotation={toggleRotation}
            />
          );
        })}
      </div>
    </div>
  );
}

export default FlightList;
