import { useEffect, useState } from "react";
import "./App.css";
import AircraftList from "./components/AircraftList";
import AircraftTimeline from "./components/AircraftTimeline";
import ScheduledFlightList from "./components/ScheduledFlightList";
import FlightList from "./components/FlightList";

function App() {
  const baseUrl = "https://infinite-dawn-93085.herokuapp.com";

  const [aircrafts, setAircrafts] = useState([]);
  const [flights, setFlights] = useState([]);

  const [availableFlights, setAvailableFlights] = useState(flights);
  const [scheduledFlights, setScheduledFlights] = useState([]);
  const [isInvalid, setIsInvalid] = useState(false);

  async function fetchAircrafts() {
    await fetch(`${baseUrl}/aircrafts`, {
      method: "GET",
    })
      .then((response) => response.json())
      .then((data) => {
        setAircrafts([...data.data]);
      })
      .catch((error) => console.error("Error", error));
  }
  // todo: loop through pages to get all flights
  async function fetchFlights() {
    await fetch(`${baseUrl}/flights`, {
      method: "GET",
    })
      .then((response) => response.json())
      .then((data) => {
        setFlights([...data.data]);
        setAvailableFlights([...data.data]);
      });
  }

  useEffect(() => {
    fetchAircrafts();
    fetchFlights();
    setIsInvalid(false);
  }, [baseUrl]);

  const today = new Date();
  const tomorrow = new Date(today);
  tomorrow.setDate(tomorrow.getDate() + 1);

  /**
   * 20 min = 1200s
   * 24 hours = 86400s
   */
  function toggleRotation(flight) {
    setIsInvalid(false);
    const isAvailable = availableFlights.find((af) => af.id === flight.id);

    function validateTime(departuretime, arrivaltime) {
      // invalid states:
      let departureTimeExists = false;
      const groundedAtMidnight = departuretime < arrivaltime;

      let invalidTimes = scheduledFlights?.filter((sf) => {
        departureTimeExists = departuretime === sf.departuretime;
        if (departuretime > sf.departuretime) {
          if (departuretime < sf.arrivaltime) {
            return true;
          } else {
            if (departuretime - sf.arrivaltime < 1200) {
              return true;
            }
          }
        } else {
          if (sf.departuretime - arrivaltime < 1200) {
            return true;
          }
        }
        return false;
      });

      return scheduledFlights
        ? !departureTimeExists &&
            groundedAtMidnight &&
            invalidTimes.length === 0
        : false;
    }

    if (isAvailable) {
      if (validateTime(flight.departuretime, flight.arrivaltime) === true) {
        // sort scheduledFlights for viewing - by flight time or by flight number? probably flight time
        setScheduledFlights(
          [...scheduledFlights, flight].sort((a, b) =>
            a.departuretime > b.departuretime ? 1 : -1
          )
        );
        const updatedAvailableFlights = availableFlights.filter(
          (f) => f.id !== flight.id
        );

        setAvailableFlights([...updatedAvailableFlights]);
      } else {
        setIsInvalid(true);
        console.log(" is invalid");
      }
    } else {
      setAvailableFlights([...availableFlights, flight]);
      const updatedScheduledFlights = scheduledFlights.filter(
        (f) => f.id !== flight.id
      );
      // sort scheduledFlights for viewing - by flight time or by flight number? probably flight time
      setScheduledFlights(
        [...updatedScheduledFlights].sort((a, b) =>
          a.departuretime > b.departuretime ? 1 : -1
        )
      );
    }
  }

  let inFlightTimes = scheduledFlights.map((flight) => {
    let flightTime = [flight.departuretime / 1440, flight.arrivaltime / 1440]; // divide by  1400 to get minutes past midnight
    return flightTime;
  });

  console.log({ inFlightTimes });
  return (
    <div className="App">
      <div className="page-container">
        <div className="date-rotator">{tomorrow.toDateString()}</div>
        {isInvalid ? (
          <div className="error">
            Scheduled time conflicts with another flight
          </div>
        ) : null}

        <div className="content-container">
          <AircraftList aircrafts={aircrafts} flights={scheduledFlights} />
          {/* would need to update aircraft using a selectedAircraft var for multiple aircrafts, for now sticking to the simplification as instructed */}
          <div className="center-panel">
            <ScheduledFlightList
              flights={scheduledFlights}
              aircraft={aircrafts[0]?.ident}
              toggleRotation={toggleRotation}
            />
            <AircraftTimeline inFlightTimes={inFlightTimes} />
          </div>
          <FlightList
            flights={availableFlights}
            toggleRotation={toggleRotation}
            isInvalid={isInvalid}
          />
        </div>
      </div>
    </div>
  );
}

export default App;
