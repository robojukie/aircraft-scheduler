This app shows a list of aircrafts and flights. Flights can be schedule with a minimum 20 minute turnaround between the end of one flight and beginning of next.

Select flight to add to schedule by clicking on one under 'Flights'
Select flight to remove from schedule by clicking on one under 'Rotation'

For this portion, the simplifications listed in the project assignment were taken

Things to address with more time:

- finish the progress/aircraft timeline portion
- handle paginations for the API
- display flights in scrollable container so you can still view the rotation/schedule when browsing flights listed lower on page

Future features/implementations:

- handling multiple aircraft
- handling flights for multiple dates

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
